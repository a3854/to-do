# To Do


As part of the QChallenge, are the to-do's:

## Build and Push - Deploy to ECS (Challenge 2)

- Using the Git Pipeline, the container image pushes to [GitLab CR](https://gitlab.com/a3854/deploy-app-to-ecs/container_registry/).

- Configure Task Definition against the previously created ECS Cluster.

- Configure Service, Default Zone, AWS Key and Secret in the Pipeline variables.

- Configure Pipeline steps including inline bash to run the `aws ecs deploy` command.

- Run Pipeline

- Test Application


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
